<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration{

    public function up(){
        Schema::create('transactions', function(Blueprint $table){
            $table->id();
            $table->bigInteger('amount')->nullable();
            $table->text('bank_transaction_id')->nullable();
            $table->boolean('paid')->default(false);
            
            //from base project
            $table->unsignedInteger('client_id');
            $table->timestamp('transaction_date')->nullable();
            $table->timestamps();

        });
    }


    public function down(){
        Schema::dropIfExists('transactions');
    }
}
