<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = ['stock' , 'sends' , 'totalsend' , 'emergencysend'];

    protected $table = 'sms';
}
