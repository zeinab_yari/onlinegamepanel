<?php

namespace TCG\Voyager\FormFields;

class Gallery extends AbstractHandler{
    protected $codename = 'gallery';

    public function createContent($row, $dataType, $dataTypeContent, $options){
        return view('voyager::formfields.gallery', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
