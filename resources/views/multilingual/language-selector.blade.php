@if (isset($isModelTranslatable) && $isModelTranslatable)
    <div class="language-selector">
        <div class="btn-group btn-group-sm" role="group" data-toggle="buttons" style="display: block; text-align: right; margin-right: 10px">
{{--            @foreach(config('voyager.multilingual.locales') as $lang)--}}
            @foreach(\App\Models\Language::all() as $lang)

                <label class="btn btn-primary{{ ($lang->symbol === config('voyager.multilingual.default')) ? " active" : "" }} selectLangLabel"
                       style="margin: 4px;border-radius: 4px; background: #26D882 ; border-color: #26D882; padding: 5px">
                    <input type="button" name="i18n_selector" id="{{$lang->symbol}}" autocomplete="off"{{ (strtoupper($lang->symbol) === strtoupper(config('voyager.multilingual.default'))) ? ' checked="checked"' : '' }} hidden/>

                    <img src="{{ \TCG\Voyager\Facades\Voyager::image($lang->flag) }}" style="margin-right: 5px;" />
                    {{ strtoupper($lang->symbol) }}

                </label>

            @endforeach
        </div>
    </div>
@endif
