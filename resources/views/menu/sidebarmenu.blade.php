@php

    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }


@endphp


@foreach($items as $menu_item)

    @php

        if (Voyager::translatable($menu_item)) {
            $menu_item = $menu_item->translate($options->locale);
        }
        if($menu_item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$menu_item->route))) && $menu_item->route!="voyager.dashboard")
          continue;
        $links = [url($menu_item->link())];
        $permission = 0;
    @endphp

    @foreach($menu_item->children as $item)
        @php
            if($item->route!="" && !Auth::user()->hasPermission(str_replace('-','_',"browse_".str_replace(['voyager.','.index'],'',$item->route))) && $item->route!="voyager.dashboard")
              continue;
            $links[] = url($item->link());
            $permission++;
        @endphp
    @endforeach

    @if($permission == 0 && count($menu_item->children))
        @php
            continue;
        @endphp
    @endif

    @if(auth()->guard(app('VoyagerGuard'))->user()->getOlaqAttribute('browse_site') && $menu_item->title =='Admin Report')


        <li class="@if(in_array(url()->current(),$links)) active @endif @if(count($menu_item->children) > 0) sub-menu @endif">

            <a class="flex-box nav-item" @if(count($menu_item->children) > 0) href='#'
               @else href='{{$menu_item->link()}}' @endif>

                <i class="{{ $menu_item->icon_class ?? 'voyager-helm'}}"
                   style="font-size: 20px;height: 25px;margin-right: 12px;margin-left: 12px;"></i>

                {{ $menu_item->title }}

                @if(false)
                    <div class="massage-number">
                        +۹۹
                    </div>
                @endif

                @if(count($menu_item->children) > 0)
                    <svg class="arrow" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M14 8L10 12L14 16" stroke="#" stroke-width="1.5" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>
                @endif
            </a>

            @if(count($menu_item->children) > 0)
                <ul style="padding-right: 0px">
                    @foreach($menu_item->children as $item)

                        @php

                            $_role = str_replace('-','_', "browse_" . str_replace( ['voyager.' , '.index'] , '' , $item->route ));
                            if($item->role)
                                $_role = $item->role;

                            if($item->route != "" && str_contains($item->route , 'voyager.') && !Auth::user()->hasPermission($_role) && $item->route != "voyager.dashboard")
                              continue;
                            if (Voyager::translatable($item)) {
                                $item = $item->translate($options->locale);
                            }
                        @endphp

                        <li>
                            <a class="flex-box" href="{{ $item->link()}}" target="{{$item->target}}">
                                <i class="{{ $item->icon_class ?? 'voyager-helm'}}"
                                   style="font-size: 20px;height: 25px;@if(app()->getLocale() === "fa") margin-right: -28px;margin-left: 12px; @else  margin-left: -28px;margin-right: 12px; @endif"></i>
                                {{$item->title}}
                            </a>
                        </li>
                    @endforeach
                </ul>

            @endif

        </li>
    @elseif($menu_item->title !='Admin Report')
        <li class="@if(in_array(url()->current(),$links)) active @endif @if(count($menu_item->children) > 0) sub-menu @endif">

            <a class="flex-box nav-item" @if(count($menu_item->children) > 0) href='#'
               @else href='{{$menu_item->link()}}' @endif>

                <i class="{{ $menu_item->icon_class ?? 'voyager-helm'}}"
                   style="font-size: 20px;height: 25px;margin-right: 12px;margin-left: 12px;"></i>

                {{ $menu_item->title }}

                @if(false)
                    <div class="massage-number">
                        +۹۹
                    </div>
                @endif

                @if(count($menu_item->children) > 0)
                    <svg class="arrow" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M14 8L10 12L14 16" stroke="#" stroke-width="1.5" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>
                @endif
            </a>

            @if(count($menu_item->children) > 0)
                <ul style="padding-right: 0px">

                    @foreach($menu_item->children as $item)

                        @php

                            $_role = str_replace('-','_', "browse_" . str_replace( ['voyager.' , '.index'] , '' , $item->route ));
                            if($item->role)
                                $_role = $item->role;

                            if($item->route != "" && str_contains($item->route , 'voyager.') && !Auth::user()->hasPermission($_role) && $item->route != "voyager.dashboard")
                              continue;
                            if (Voyager::translatable($item)) {
                                $item = $item->translate($options->locale);
                            }
                        @endphp

                        <li>
                            <a class="flex-box" href="{{ $item->link()}}" target="{{$item->target}}">
                                <i class="{{ $item->icon_class ?? 'voyager-helm'}}"
                                   style="font-size: 20px;height: 25px;@if(app()->getLocale() === "fa") margin-right: -28px;margin-left: 12px; @else  margin-left: -28px;margin-right: 12px; @endif"></i>
                                {{$item->title}}
                            </a>
                        </li>
                    @endforeach
                </ul>

            @endif

        </li>
    @endif
@endforeach

