<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" id="{{ config('app.locale') == 'fa' ? 'rtl' : 'ltr' }}">
<head>

    <meta charset="UTF-8">
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

{{--    <link href="{{ voyager_asset('style/app.css') }}" rel="stylesheet">--}}
    <link href="{{ voyager_asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ voyager_asset('css/master.css') }}&x=5165" rel="stylesheet">
    <link href="{{ voyager_asset('css/voyager-font.css') }}" rel="stylesheet">
    <link href="{{ voyager_asset('style/newStyle.css') }}" rel="stylesheet">
    <script src="{{ voyager_asset('js/JQUERY.js') }}"></script>
    <script src="{{ voyager_asset('js/BOOTSTRAP.js') }}"></script>
    <script src="{{ voyager_asset('js/ajax.js') }}"></script>
    <script src="{{ voyager_asset('js/chart.js') }}"></script>

    <!--    --><?php //$admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    {{--@if($admin_favicon == '')
        <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">
    @else
        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif--}}

    @yield('css')

    @if(!empty(config('voyager.additional_css')))
        @foreach(config('voyager.additional_css') as $css)
            <link rel="stylesheet" type="text/css" href="{{ asset($css) }}">
        @endforeach
    @endif

    <style>
        .mainHeaderDashboardMainBox{
            width: calc(100% - 20px);
            background-image: linear-gradient(to right, #2a5ff5 , #2b95f5);
            display: flex;
            align-items: center;
            justify-content: space-between;
            border-radius: 5px;
            padding: 10px 15px;
            margin: auto;
            margin-bottom: 16px;
        }

        .MHD-icon{
            width: 40px;
        }

        .MHD-icon div{
            width: 100%;
            height: 2px;
            background-color: #fff;
            margin-bottom: 8px;
        }

        .MHD-icon div:last-child{
            margin-bottom: 0;
        }

        .MHD-Title{
            color: #fff;
            font-size: 19px;
        }
    </style>

    @yield('head')

</head>

<body>

<div class="container-fluid">
    <div class="overlay">

    </div>
    <div class="row ">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="container">
                @include('voyager::dashboard.sidebar')
                <div class="main">
                    <div id="nav-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="row">
                        @yield('box_header')

                        <div class="mainHeaderDashboardMainBox">
                            <div class="MHD-icon">
                                <div></div>
                                <div></div>
                            </div>
                            <div class="MHD-Title">@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</div>
                            <div class="MHD-icon">
                                <div></div>
                                <div></div>
                            </div>
                        </div>

                        @yield('search')

                        {{--<div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <nav class="navigation">
                                @include('voyager::dashboard.navbar')
                                <div class="right-top-nav">
                                    <form>
                                        <img class="arrow" src="{{ voyager_asset('icon/arrow.svg') }}">
                                        <img class="search" src="{{ voyager_asset('icon/search.svg') }}">
                                        <div class="search-box">
                                            <input class="search-input" type="text"
                                                   placeholder="جستجو در لیست کاربران ">
                                            <ul>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در تراکنش ها</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                             نام کاربر
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در محصولات</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           نام محصول
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در ویترین</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           عنوان ویترین
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در سرویس ها</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           عنوان سرویس
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </form>
                                </div>
                            </nav>
                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="row">

                                @yield('content')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ voyager_asset('js/master.js') }}&x=5467"></script>
<script src="{{ voyager_asset('js/dropdown.js') }}"></script>
<script src="{{ voyager_asset('js/nprogress.js') }}"></script>


<script
    src="{{ Auth::user()->locale ? voyager_asset('js/main_'.Auth::user()->locale.'.js') : voyager_asset('js/main_'.Config::get('app.locale').'.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>
<script src="{{ voyager_asset('js/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<script src="{{ voyager_asset("js/multilingual.js") }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js"></script>

<script>
    $("img").click(function (){
        if(!$(this).parent().is('a')){
            var win = window.open($(this).attr('src'), '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        }

    })

</script>


@include('voyager::media.manager')

@yield('javascript')

@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
    @foreach(config('voyager.additional_js') as $js)
        <script type="text/javascript" src="{{ voyager_asset($js) }}"></script>
    @endforeach
@endif

</body>
</html>
