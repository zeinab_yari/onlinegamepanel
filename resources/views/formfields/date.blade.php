

<div>
    <input type="date" class="form-control datePickerInput" name="{{ $row->field }}"
           placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
           value="@if(isset($dataTypeContent->{$row->field})){{ \Carbon\Carbon::parse(old($row->field, $dataTypeContent->{$row->field}))->format('Y-m-d') }}@else{{old($row->field)}}@endif">


    <img class="black-calender" src="{{voyager_asset('icon/black-calender.svg')}}">
    <img class="delet-value" src="{{voyager_asset('icon/delet-value.svg')}}">

</div>
