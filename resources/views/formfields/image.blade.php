{{--
<div class="imageupload">
    --}}
<!--<div>
    <div class="thumbnail" data-field-name="{{ $row->field }}">
        <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
        <img
            src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
            data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}"
            style="max-width:200px;min-width:200px; height:auto; clear:both; display:block; padding:2px;">
        <button class="uploadavatar"></button>

    </div>
</div>-->

<!--

</div>-->

<div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
    <div class="input-row">
        <label id="fileimage" for="upload-imge-{{ $row->field }}" class="upload-file menu-items flex-box">
        <span class="flex-box">
            <img src="{{ voyager_asset('icon/upload-image.svg') }}">
            Upload Photo
        </span>
            <p>
                File Selection
            </p>

            <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
                   name="{{ $row->field }}" id="upload-imge-{{ $row->field }}" class="uploadImageInput" data-row-id="{{ $row->field }}"
                   data-preview="preview{{ $row->field }}Pic" hidden accept="image/*">
            {{--        <input type="file" class="" multiple="" hidden="">--}}
        </label>
    </div>
    <div class="upload-image-row file-row margin-bottom preview{{ $row->field }}Pic">
    <!--
        <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
        <img
            src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
            data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}"
            style="max-width:200px;min-width:200px; height:auto; clear:both; display:block; padding:2px;">
-->


        @if($dataTypeContent->{$row->field})
            <div class="acsses image-file files" id="files{{ $row->field }}">

                <div class="flex-box">
                    <img class="close-tag" src="{{ voyager_asset('icon%2Fclos.svg') }}" onclick="$('#files{{ $row->field }}').remove()">
                    <div class="image-box">
                        <img id="upload-image-item"
                             src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}
                             @else{{ $dataTypeContent->{$row->field} }}@endif">
                    </div>
                </div>
                <div class="flex-box upload-prosses"></div>
            </div>
        @endif
    </div>
</div>

