@extends('voyager::master')

@section('css')

    @include('voyager::compass.includes.styles')

@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-compass"></i>
        <p> {{ __('voyager::generic.compass') }}</p>
    </h1>
@stop

@section('content')

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                        <h5 class="title">
                            compass
                        </h5>
                    </div>
                    <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                        <div class="tabs">
                            <ul class="tabs__button-group">
                                <li><button class="tabs__toggle @if(empty($active_tab) || (isset($active_tab) && $active_tab == 'fonts'))active @endif" data-tab="1">{{ __('voyager::compass.resources.title') }}</button></li>
                                <li><button class="tabs__toggle @if($active_tab == 'logs')active @endif" data-tab="2">{{ __('voyager::compass.logs.title') }}</button></li>

                            </ul>
                            <ul class="tabs__container box setting-row">
                                <li class="tabs__tab-panel @if(empty($active_tab) || (isset($active_tab) && $active_tab == 'fonts'))active @endif" data-tab="1">
                                    <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                        <div class="box ">
                                            <div class="row">
                                    @include('voyager::compass.includes.fonts')
                                            </div>
                                        </div>
                                    </div>

                                </li>
                                <li class="tabs__tab-panel @if($active_tab == 'logs') active @endif" data-tab="2">
                                    @include('voyager::compass.includes.logs')

                                </li>


                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

<script>
    class Tabs {
        constructor(element) {
            this.tabs = element;
            this.toggles = this.tabs.querySelectorAll('.tabs__toggle');
            this.panels = this.tabs.querySelectorAll('.tabs__tab-panel')
        }
        init() {
            this.toggles.forEach(toggle => {
                toggle.addEventListener('click', (e) => {
                    this.toggles.forEach(toggle => {
                        toggle.classList.remove('active');
                    })
                    this.panels.forEach(panel => {
                        panel.classList.remove('active');
                    })
                    e.target.classList.add('active');
                    this.tabs.querySelector(`.tabs__tab-panel[data-tab='${e.target.dataset.tab}']`).classList.add('active')
                })
            })
        }
    }

    document.querySelectorAll('.tabs').forEach(tab =>{
        const tabs = new Tabs(tab);
        tabs.init();
    })
</script>

<script>
    $(".account-number").click(function () {
        if ($(this).attr('title') == 'حذف') {
            var idModal = "/" + $(this).attr("data-id");
            $("#delete_form").attr('action', window.location.href + idModal)
        }
    });


    $('#upload-file-btn').on('change',function(e){
        var fileName = e.target.files[0].name;

        $( ".upload-file-row2" ).append( "<div class=\"acsses  files\">" +
            "<div class=\"flex-box\">" +
            "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg') }}\">"
            + fileName +
            "</div>" +
            "<div class=\"flex-box upload-prosses\">" +
            "</div>" +
            "</div>" );
        $('.close-tag').click(function(){
            $(this).parent().parent().fadeOut();
        });

    });
    $('.delet-file').click(function(){
        $('.upload-file-row2 .files').fadeOut();
    });


    $('#upload-file').on('change',function(){
        var filename = $(this).val().replace(/.*(\/|\\)/, '');
        $( ".upload-file-row" ).append( "<div class=\"acsses  files\">" +
            "<div class=\"flex-box\">" +
            "<img class='close-tag' src=\"{{ voyager_asset('icon/clos.svg')}}\">"
            + filename  +
            "</div>" +
            "<div class=\"flex-box upload-prosses\">" +
            "</div>" +
            "</div>" );
        $('.close-tag').click(function(){
            $(this).parent().parent().fadeOut();
        });
    });

</script>



@stop
@section('javascript')
    <script>
        $('document').ready(function(){
            $('.collapse-head').click(function(){
                var collapseContainer = $(this).parent();
                if(collapseContainer.find('.collapse-content').hasClass('in')){
                    collapseContainer.find('.voyager-angle-up').fadeOut('fast');
                    collapseContainer.find('.voyager-angle-down').fadeIn('slow');
                } else {
                    collapseContainer.find('.voyager-angle-down').fadeOut('fast');
                    collapseContainer.find('.voyager-angle-up').fadeIn('slow');
                }
            });
        });
    </script>
    <!-- JS for commands -->
    <script>

        $(document).ready(function(){
            $('.command').click(function(){
                $(this).find('.cmd_form').slideDown();
                $(this).addClass('more_args');
                $(this).find('input[type="text"]').focus();
            });

            $('.close-output').click(function(){
                $('#commands pre').slideUp();
            });
        });

    </script>

    <!-- JS for logs -->
    <script>
      $(document).ready(function () {
        $('.table-container tr').on('click', function () {
          $('#' + $(this).data('display')).toggle();
        });
        $('#dataTable').DataTable({
          "order": [1, 'desc'],
          "stateSave": true,
          "dom": '<"filters"f><"datatableserv"rt><"bottom"pli><"clear">',
          "language": {!! json_encode(__('datatable')) !!},
          "stateSaveCallback": function (settings, data) {
            window.localStorage.setItem("datatable", JSON.stringify(data));
          },
          "stateLoadCallback": function (settings) {
            var data = JSON.parse(window.localStorage.getItem("datatable"));
            if (data) data.start = 0;
            return data;
          }
        });
        $('#delete-log, #delete-all-log').click(function () {
          return confirm('{{ __('voyager::generic.are_you_sure') }}');
        });
      });
      $(document).on( 'preInit.dt', function (settings, json) {
          $('div.dataTables_length select').select2();
          $('div.dataTables_length').addClass('form-group').addClass('select');
          $('div.dataTables_length label').addClass('select');
          $('.dataTables_wrapper .dataTables_filter').addClass('form-group').addClass('search');
          $('.dataTables_wrapper .dataTables_filter input[type=search]').attr('placeholder','جستجو');
          $('.table-responsive').addClass('notserverside');
      });
    </script>
@stop
