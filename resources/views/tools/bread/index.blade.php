@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.__('voyager::generic.bread'))


@section('css')

    <style>

        .in {
            opacity: 1 !important;
            background-color: #00000040;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <h5 class="title">
                Breads
            </h5>
        </div>
        <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
            <div class="box">

                <div class="row Breads margin-bottom">
                        <div class=" padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title-rows flex-box justify-content-between">
                                <p>
                                    {{ __('voyager::database.table_name') }}
                                </p>
                                <div class="left-item">
                                    <p class="text-center">
                                        {{ __('voyager::bread.bread_crud_actions') }}

                                    </p>
                                </div>

                            </div>
                        </div>

                        @foreach($tables as $table)
                            @continue(in_array($table->name, config('voyager.database.tables.hidden', [])))
                            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                <div draggable="true" class="menu-items flex-box">
                                    <p style="color: inherit"
                                       href="{{ route('voyager.database.show', $table->prefix.$table->name) }}"
                                       data-name="{{ $table->prefix.$table->name }}">
                                        {{ $table->name }}
                                    </p>

                                    @if($table->dataTypeId)
                                        <div class="left-item flex-box  justify-content-between">

                                            <a class="account-number flex-box "
                                               href="{{ route('voyager.' . $table->slug . '.index') }}">
                                                <img src="{{voyager_asset('icon/see.svg')}}">

                                            </a>
                                            <a class="account-number flex-box "
                                               href="{{ route('voyager.bread.edit', $table->name) }}">
                                                <img src="{{voyager_asset('icon/Edit.svg')}}">
                                            </a>
                                            <a class="account-number flex-box delete-bread"
                                               href="#delete-bread" data-id="{{ $table->dataTypeId }}"
                                               data-name="{{ $table->name }}">
                                                <img src="{{voyager_asset('icon/trash.svg')}}">
                                            </a>

                                        </div>


                                    @else

                                        <div class="left-item flex-box  justify-content-between">
                                            <a class="add-breads account-number flex-box "
                                               href="{{ route('voyager.bread.create', $table->name) }}">
                                                <img src="{{voyager_asset('icon/add.svg')}}">
                                                {{ __('voyager::bread.add_bread') }}
                                            </a>
                                        </div>
                                    @endif


                                </div>
                            </div>
                        @endforeach

                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade show" tabindex="-1" id="delete_builder_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {!! __('voyager::bread.delete_bread_quest', ['table' => '<span id="delete_builder_name"></span>']) !!}
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_builder_form" method="POST">
                        {{ method_field('DELETE') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="submit" class="flex-box add-new" style="border: none"
                               value="{{ __('voyager::bread.delete_bread_conf') }}">
                    </form>
                    <button type="button" class="menu-items close"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop


@section('javascript')

    <script>

        var table = {
            name: '',
            rows: []
        };

        new Vue({
            el: '#table_info',
            data: {
                table: table,
            },
        });

        $(function () {

            // Setup Delete BREAD
            //
            $('.delete-bread').on('click', function (e) {

                id = $(this).data('id');
                name = $(this).data('name');

                $('#delete_builder_name').text(name);
                $('#delete_builder_form')[0].action = '{{ route('voyager.bread.delete', ['__id']) }}'.replace('__id', id);
                $('#delete_builder_modal').modal('show');
            });

            // Setup Show Table Info
            //
            $('.database-tables').on('click', '.desctable', function (e) {
                e.preventDefault();
                href = $(this).attr('href');
                table.name = $(this).data('name');
                table.rows = [];
                $.get(href, function (data) {
                    $.each(data, function (key, val) {
                        table.rows.push({
                            Field: val.field,
                            Type: val.type,
                            Null: val.null,
                            Key: val.key,
                            Default: val.default,
                            Extra: val.extra
                        });
                        $('#table_info').modal('show');
                    });
                });
            });
        });
    </script>
    <script>
        $("#select-all").click(function () {
            $("input[type=checkbox]").prop('checked', $(this).prop('checked'));

        });

        $(".my-checkbox:checkbox").click(function () {
            if ($('.my-checkbox:checkbox').is(":checked")) {
                $(' .hide').css("display", "none")
                $(' .red-item').css("display", "inline-flex")
            } else {
                $(' .hide').css("display", "inline-flex")
                $(' .red-item').css("display", "none")

            }
        });


        $(".select-all").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".select-all ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".select-all ul").hide();
            $(".select-all").removeClass("active")
        });


        $(".more-row .more-item").click(function (e) {
            $(this).children("ul").toggle();
            $(this).toggleClass("active")
            e.stopPropagation();


        });

        $(".more-row .more-item ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".more-row .more-item ul").hide();
            $(".more-row .more-item").removeClass("active")
        });
    </script>
    <script>

        $(".list-filter").click(function (e) {
            if ($(this).hasClass('active')) {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
            } else {
                $(".list-filter ul").hide();
                $(" .list-filter ").removeClass("active")
                $(this).addClass("active")
                $(this).children("ul").toggle();
            }


            e.stopPropagation();


        });

        $(".list-filter ul").click(function (e) {
            e.stopPropagation();
        });

        $(document).click(function () {
            $(".list-filter ul").hide();
            $(" .list-filter ").removeClass("active")
        });
    </script>
@stop

